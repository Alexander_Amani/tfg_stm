/*
 * AMP.h
 *
 *  Created on: 19 jun. 2021
 *      Author: halex
 */

#ifndef INC_AMP_H_
#define INC_AMP_H_

#include "stm32f4xx_hal.h"

typedef struct{
	float scale; //factor de escala de voltios a gramos
	float offset; //offset de la lectura en voltios

	ADC_HandleTypeDef* hadc; //handle del adc
	uint32_t canal; //canal al que está conectado del adc
	uint16_t resolucion; //resolución del conversor

	float vref; //voltaje de referencia del adc (3.0V tipicamente)
	uint16_t* pvref; //puntero a la dirección que contiene el valor necesario para calibrar la tensión de referencia

}AMP;

void AMPInit(AMP* ampl, ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t resolucion, uint16_t* pvref); //iniciamos la estructura
//como es una estructura, en principio, no hace falta funciones set y get
uint32_t leerADC(ADC_HandleTypeDef* hadc, uint32_t time); //función para leer del adc por polling
void CanalADC(ADC_HandleTypeDef* hadc, uint32_t CANAL); //función para cambiar de canal en un adc
float vrefADC(ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t* pvref, uint8_t n); //función para obtener la tensión de referencia
float shiftAMP(uint32_t adc_val, float vref); //función que nos da el valor antes de ser desplazado
uint32_t leerADCMedia(ADC_HandleTypeDef* hadc, uint32_t time, uint32_t n); //lectura de n veces
float lecturaAMP(AMP* ampl, uint32_t time, uint32_t n); //lectura del amplificador desplazada y con offset y pendiente

#endif /* INC_AMP_H_ */
