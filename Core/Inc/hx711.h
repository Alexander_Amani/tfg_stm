/*
 * hx711.h
 *
 *  Created on: Apr 23, 2021
 *      Author: Alexander Amani
 *      Description: pequeña "libreria" para módulo hx711 en stm32
 */

#ifndef INC_HX711_H_
#define INC_HX711_H_

#include "stm32f4xx_hal.h"

typedef struct {
	/*
	 *
	 * */
	//puertos a los pines DT Y SCK:
	GPIO_TypeDef* gpioDT;
	GPIO_TypeDef* gpioSCK;
	//Número del pin DT y SCK:
	uint16_t pinDT;
	uint16_t pinSCK;
	//ganancia de A o B:
	uint8_t gain; //ganacia: (128 32 64) pulsos: (25 26 27) puerto: (A B A)
	uint8_t pulses; //numero de pulsos para la señal
	//datos para calculo de peso:
	int32_t offset; //valor de offset del sensor para 0g
	float m; //pendiente g/bits
}hx711;

void hx711Init(hx711*); //inicializamos
void hx711SetGain(hx711*,uint8_t); //definimos la ganancia y realizamos una lectura (sino no funciona)
int32_t hx711Read(hx711*); //leemos
void hx711SetOffset(hx711*, uint8_t); //ponemos el valor del offset con media n
void hx711Setm(hx711*,float, uint8_t); //en funcion del peso metido, calculamos la pendiente con media n
int32_t hx711ReadMean8(hx711*); //Media con 8 muestras
int32_t hx711ReadMean(hx711*,uint8_t); //lectura con media (hasta 255)
float hx711Weight(hx711*, uint8_t); //devuelve el peso tras n medidas

#endif /* INC_HX711_H_ */
