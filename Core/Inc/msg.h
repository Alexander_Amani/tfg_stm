/*
 * msg.h
 *
 *  Created on: May 5, 2021
 *      Author: Alexander Amani
 */

#ifndef INC_MSG_H_
#define INC_MSG_H_

#include "stm32f4xx_hal.h"
#include "string.h"
#define BUFFSIZE 255 //tamaño maximo del buffer


typedef struct{
	char				rxBuffer[BUFFSIZE];	//buffer de almacenamiento de los caracteres que llegan por UART
	uint8_t				n;					//contador de bits que hay almacenados en el buffer
	char				start;				//char de inicio del mensaje ('#' por ejemplo)
	char				end;				//char de fin del mensaje ('/n' por ejemplo)
	volatile uint8_t	endFlag;			//banderilla de mensaje completo
	volatile uint8_t	state;				//estado
}msg;

void msgInit(msg* m, char start, char end); //inicializamos la estructura

uint8_t msgEndFlag(msg* m); //comprobamos si mensaje disponible

void msgBuffer(msg* m, char c); //llenamos el buffer

void msgRead(msg* m, char* pntr); //lee el mensaje que haya en el buffer

#endif /* INC_MSG_H_ */
