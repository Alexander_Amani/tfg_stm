/*
 * AMP.c
 *
 *  Created on: 19 jun. 2021
 *      Author: halex
 */

#include "AMP.h"

void AMPInit(AMP* ampl, ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t resolucion, uint16_t* pvref){
	/*Aquí se inicializa la estructura del AMP
	 * */

	//metemos los valores del hadc, del canal, la resolución del adc y el puntero pvref:
	ampl->hadc=hadc;
	ampl->canal=canal;
	ampl->resolucion=resolucion; // 4095 tipicamente
	ampl->pvref=pvref; //puntero a dirección con valor para calibración vref
	//ponemos los valores del offset y scale:
	ampl->offset=0.0f;
	ampl->scale=1.0f;
	//calibramos el vref con 10 lecturas:
	ampl->vref=vrefADC(ampl->hadc, ampl->canal, ampl->pvref, 10);
}

uint32_t leerADC(ADC_HandleTypeDef* hadc, uint32_t time){
	//función para leer por poll:
	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, time);
	return HAL_ADC_GetValue(hadc); //devolvemos el valor leido
}

void CanalADC(ADC_HandleTypeDef* hadc, uint32_t CANAL){
	/*Con esta funcion se pretende cambiar el canal de un adc
	 * en un principio solo está pensado para adc con 1 solo canal
	 * */

	//estructura de inicio de canal:
	ADC_ChannelConfTypeDef sConfig = {0};

	sConfig.Channel = CANAL;
	sConfig.Rank = 1; //se supone que es solo de 1 canal luego siempre mismo rank
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES; //siempre mismos ciclos
	if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
	{
	   Error_Handler();
	}
}

float vrefADC(ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t* pvref, uint8_t n){
	//parece ser que solo el ADC1 puede leer ese canal:

	float res=0;

	//cambiamos el canal al del vref:
	CanalADC(hadc, ADC_CHANNEL_VREFINT);
	//leemos n veces:
	for(uint8_t i=0; i<n; i++){
		res=res+(3.3f*(*pvref)/leerADC(hadc, HAL_MAX_DELAY)); //fórmula predefinida
	}
	res=res/n;

	//devolvemos el adc a su canal:
	CanalADC(hadc, canal);

	return res;

}

float shiftAMP(uint32_t adc_val, float vref){
	//obtenemos el valor en V del amplificador:
	float res;

	res=vref*((2.0f*adc_val/4095)-1);

	return res;
}

uint32_t leerADCMedia(ADC_HandleTypeDef* hadc, uint32_t time, uint32_t n){
	/*leemos n veces del adc en un canal determinado
	 * */
	uint32_t res=0; //resultado final

	//leemos n veces:
	for(uint32_t i=0; i<n; i++){
		res+=leerADC(hadc,time);
	}
	res=res/n;

	return res;
}

float lecturaAMP(AMP* ampl, uint32_t time, uint32_t n){
	/*Primero leemos n veces y después obtenemos el valor desplazado, con offset y pendiente:
	 * */
	uint32_t vin;
	float res;

	vin=leerADCMedia(ampl->hadc, time, n);
	res=shiftAMP(vin, ampl->vref);
	//offset y factor de escala:
	res=ampl->scale*(res-ampl->offset);

	return res;
}
