/*
 * hx711.c
 *
 *  Created on: Apr 23, 2021
 *      Author: Alexander Amani
 */

#include "hx711.h"

void hx711Init(hx711* h){

}
void hx711SetGain(hx711* h, uint8_t gain){
	/*
	 * En funcion de la ganancia, y del puerto A o B se elige el número de pulsos de señal
	 * Para hacer efectivo este cambio hay que realizar una primera lectura
	 * */
	switch (gain) {
		case 128:
			h->gain=gain;
			h->pulses=25; //valor de datasheet
			break;
		case 32:
			h->gain=gain;
			h->pulses=26; //valor de datasheet
			break;
		case 64:
			h->gain=gain;
			h->pulses=27; //valor de datasheet
			break;
		default:
			h->gain=128;
			h->pulses=25; //valor de datasheet
			break;
	}
	//Realizamos lectura para confirmar el cambio:
	hx711Read(h);
}
int32_t hx711Read(hx711* h){
	uint32_t buffer=0; //aquí se almacenan los bits que van llegando
	int32_t val=0; //valor final

	while(HAL_GPIO_ReadPin(h->gpioDT, h->pinDT)); //hasta que no esté listo no leemos
	for(int i=0; i<24; i++){ //al menos 25 pulsos para leer
		HAL_GPIO_WritePin(h->gpioSCK, h->pinSCK, SET); //ponemos a 1
		buffer=(buffer<<1); //desplazamos 1 a la izquierda
		HAL_GPIO_WritePin(h->gpioSCK, h->pinSCK, RESET); //ponemos a 0
		if(HAL_GPIO_ReadPin(h->gpioDT, h->pinDT)){
			buffer++; //si leemos un 1, sumamos 1
		}
	}
	for(int i=24; i<(h->pulses); i++){ // en funcion de la ganancia seleccionada
		HAL_GPIO_WritePin(h->gpioSCK, h->pinSCK, SET); //ponemos a 1
		HAL_GPIO_WritePin(h->gpioSCK, h->pinSCK, RESET); //ponemos a 0
	}

	//Convertimos los bits en entero C2:
	/*
	//METODO 1 PARA C2:
	if((buffer&(1<<23))){ //si bit de signo 1
		buffer=buffer^0xFFFFFF; //le damos la vuelta
		buffer++; //sumamos 1
		val=-1*((int32_t)buffer);
	}
	else{
		val=buffer; //guardamos en otra variable para que no cambie constantemente
	}*/
	//METODO 2 C2:
	buffer=buffer<<(32-24);//Desplazamos el bit de signo (24bit) a la posicion 32:
	val=((int32_t)buffer)>>(32-24); //convertimos a entero y desplazamos a derechas
	return val;
}

void hx711SetOffset(hx711* h, uint8_t n){
	//aqui se guarda el valor del offset tras leer varias veces:
	h->offset=hx711ReadMean(h, n);
}

int32_t hx711ReadMean(hx711* h,uint8_t n){
	//en esta funcion se lee y se realiza la media de las muestras:
	int32_t tmp=0; //buffer de la media
	int32_t res; //resultado

	for (uint8_t i = 0; i < n; i++) {
		tmp+=hx711Read(h); //vamos sumando en el buffer de lecturas
	}

	res=tmp/n; //dividimos entre el numero de muestras

	return res;
}

int32_t hx711ReadMean8(hx711* h){
	//realizamos la media pero de solo 8 de forma más eficiente:
	//parece ser que los compiladores son más listos, igual no es tan eficiente
	int32_t tmp=0;
	int32_t res;

	for (uint8_t i = 0;  i < 8; i++) {
		tmp+=hx711Read(h);
	}

	res=(tmp>>3); //desplazamos 3 posicion (2^3=8)
	return res;
}

void hx711Setm(hx711* h,float g, uint8_t n){
	//con el peso en gramos y el numero de muestras para la media se calcula la pendiente para el peso:
	int32_t tmp; //almacenamos aqui la lectura en bits cuando ponemos g gramos
	float res; //resultado final

	tmp=hx711ReadMean(h,n); //obtenemos la lectura con g gramos
	tmp=tmp-h->offset; //le quitamos el offset a la medida

	res=(float)g/tmp; //calculamos la pendiente

	h->m=res; //almacenamos el valor de la pendiente
}

float hx711Weight(hx711* h, uint8_t n){
	//funcion que devuelve el valor de peso tras haber realizado n medidas
	//teniendo en cuenta el valor del offset y de la pendiente
	int32_t tmp; //valor medido del sensor
	float res;//resultado

	tmp=hx711ReadMean(h, n); //leemos n veces
	res=(float)(h->m)*(tmp-(h->offset)); //calculamos el peso en gramos

	return res;
}

