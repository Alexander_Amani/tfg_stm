/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "hx711.h"
#include "msg.h"

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "AMP.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum{REPOSO, HX, OHM, INA}estados;  //estados del sistema
typedef enum{
  REPOSO_CHAR=(int)'r', //ir a estado de reposo
  HX_CHAR=(int)'h', //ir al estado de toma de datos del HX711
  HX_TARA=(int)'t', //realizamos la tara (igual quitamos esto)
  HX_SCALE=(int)'s', //realizamos el calibrado automatico del factor de escala (igual se quita esto)
  INA_CHAR=(int)'i', //ir al estado del amplificador

  OFFSET_CHAR=(int)'o', //ajuste manual del offset (slider)
  FACTOR_CHAR=(int)'f', //ajusto manual del factor de escala (slider)
  RESET_CHAR=(int)'z', //se pone a 0 el offset y a 1 el factor de escala
  GAIN_CHAR=(int)'g', //cambiamos la ganancia entre 128 o 64
  CONECT_CHAR=(int)'c' //mensaje para comprobar si estamos conectados al micro correctamente
}charMandato; //enum con los caracteres que definen el estado

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define R 980.0f //resistencia para el Ohmnimetro

#define VREFINT_CAL_ADDR                0x1FFF7A2A //direccion del valor de vref con 3.3V de Vref
#define VREFINT_CAL ((uint16_t*) VREFINT_CAL_ADDR) //puntero a direccion que contiene el valor
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
float peso=0; //almacenamos el peso
float resis=0; //almacenamos el valor de la resistencia
float vina=0; //voltaje del ina

hx711 hx; //el hx711


msg consigna; //el mensaje con su libreria
char mensaje[255]; //buffer para el mensaje que enviamos
char c; //para el uart

estados estado=REPOSO; //empezamos en reposo
estados nextEstado=REPOSO;

AMP myAMP;

char msgConect[]="CONECTADO\n";
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
void delay_us (uint16_t us);
void accion(char* mandato); //si llega un mensaje aqui se procesa
estados siguienteEstado(char* mandato, estados estado);
void salidas(estados estado);
float Ohmnimetro(uint32_t adc_val, uint32_t Rserie);
/*
uint32_t leerADC(ADC_HandleTypeDef* hadc, uint32_t time);
void CanalADC(ADC_HandleTypeDef* hadc, uint32_t CANAL);
void SelecADC(ADC_HandleTypeDef* hadc, estados siguienteEstado);
float vrefADC(ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t* pvref, uint8_t n);
*/
float salidaINA(uint32_t adc_val, float vref);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void delay_us (uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&htim2,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim2) < us);  // wait for the counter to reach the us input in the parameter
}

void accion(char* mandato){
	switch (mandato[0]) {
		case 't':
			//si recibimos una T se realiza el ajuste del offset
		HAL_UART_Transmit(&huart2, (uint8_t*)"calibrando offset...\n", strlen("calibrando offset...\n"), HAL_MAX_DELAY);
		hx711SetOffset(&hx, 30);
			break;
		case 's':
		//si recibimos una s calibramos la pendiente usando el peso en gramos que nos hayan pasado(i.e. S215.5)
		HAL_UART_Transmit(&huart2, (uint8_t*)"calibrando pendiente...\n", strlen("calibrando pendiente...\n"), HAL_MAX_DELAY);
		hx711Setm(&hx, atoff(&mandato[1]), 30); //convertimos el string en float a partir de 1
			break;
		default:
			break;
	}
}
estados siguienteEstado(char* mandato, estados estado){
	//en funcion del mensaje recibido y del estado actual calculamos el siguiente estado:
	switch (estado) {
		case REPOSO: //estado de reposo no hacemos nada
			switch (mandato[0]) {
				case (char)HX_CHAR://señal de hx711
					return HX;// pasamos a estado de lectura de HX
					break;
				/*case 'o'://medida de resistencia
					return OHM;
					break;*/
				case (char)INA_CHAR: //INA
					return INA;
					break;
				case (char)CONECT_CHAR:
						//devolvemos mensaje de conexión:
						HAL_UART_Transmit(&huart2, (uint8_t*)msgConect, strlen(msgConect), HAL_MAX_DELAY);
						return REPOSO;
						break;
				default:
					return REPOSO; //si llega cualquier otra cosa sigue en reposo
					break;
			}
			break;

		case HX: //lectura del HX711
			switch (mandato[0]) {
			//creo que para los estados fugaces lo mejor es realizar la accion directamente aquí:
				case (char)HX_TARA: //señal de tara
					//si recibimos una T se realiza el ajuste del offset
					HAL_UART_Transmit(&huart2, (uint8_t*)"calibrando offset...\n", strlen("calibrando offset...\n"), HAL_MAX_DELAY);
					hx711SetOffset(&hx, 30);
					return HX;
					break;
				case (char)HX_SCALE:
					//si recibimos una s calibramos la pendiente usando el peso en gramos que nos hayan pasado(i.e. S215.5)
					HAL_UART_Transmit(&huart2, (uint8_t*)"calibrando pendiente...\n", strlen("calibrando pendiente...\n"), HAL_MAX_DELAY);
					hx711Setm(&hx, atoff(&mandato[1]), 30); //convertimos el string en float a partir de 1
					return HX;
					break;
				case (char)REPOSO_CHAR: //mensaje de reposo
					return REPOSO; //pasamos a estado de reposo
					break;
				/*case 'o': //medida de resistencia
					return OHM;
					break;*/
				case (char)OFFSET_CHAR:
						//ajustamos el offset manualmente:
						hx.offset=atoi(&mandato[1]);
						return HX;
						break;
				case (char)FACTOR_CHAR:
						//ajustamos el factor de escala manualmente:
						//ENVIAMOS EL INVERSO CUIDADO: 1/m:
						hx.m=1.0f/atoff(&mandato[1]);
						return HX;
						break;
				case (char)RESET_CHAR:
						//offset a 0 y pendiente a 1:
						hx.offset=0;
						hx.m=1.0f;
						return HX;
						break;
				case (char) GAIN_CHAR:
						//primero reset de offset y m:
						hx.offset=0;
						hx.m=1.0f;
						//cambiamos la ganancia (128 64):
						hx711SetGain(&hx, (uint8_t)atoi(&mandato[1]));
						return HX;
						break;
				case (char) CONECT_CHAR:
						//mandamos mensaje de conexión:
						HAL_UART_Transmit(&huart2, (uint8_t*)msgConect, strlen(msgConect), HAL_MAX_DELAY);
						return HX;
						break;
				case (char)INA_CHAR: //INA
					return INA;
					break;
				default:
					//si me mandan cualquier otra cosa:
					return HX; //nos quedamos donde estamos
					break;
			}
			break;

		case OHM:
			switch (mandato[0]) {
				case 'r': //estado de reposo
					return REPOSO;
					break;
				case 'h': //estado HX711
					return HX;
					break;
				case 'i': //INA
					return INA;
					break;
				default:
					//si me mandan cualquier otra cosa:
					return OHM;
					break;
			}
			break;

		case INA: //INA u otro op amp
			switch (mandato[0]) {
				case (char)REPOSO_CHAR: //estado de reposo
					return REPOSO;
					break;
				case (char)HX_CHAR: //HX711
					return HX;
					break;
				/*case 'o': //Ohmnimetro
					return OHM;
					break;*/
				case (char)OFFSET_CHAR:
						//se ajusta el offset manualmente:
						myAMP.offset=atoff(&mandato[1]);
						return INA;
						break;
				case (char)FACTOR_CHAR:
						//se ajusta el factor de escala manualmente:
						myAMP.scale=atoff(&mandato[1]);
						return INA;
						break;
				case (char)RESET_CHAR:
						//se pone offset a 0 y scale a 1:
						myAMP.offset=0.0f;
						myAMP.scale=1.0f;
						return INA;
						break;
				case (char)CONECT_CHAR:
						//devolvemos mensaje de conexión:
						HAL_UART_Transmit(&huart2, (uint8_t*)msgConect, strlen(msgConect), HAL_MAX_DELAY);
						return INA;
						break;
				default:
					return INA;
					break;
			}
			break;

		default: //es raro que esté en otro estado, pero por si acaso:
			return estado;
			break;
	}
	//si por algun casual llegamos hasta aquí:
	return estado;
}

void salidas(estados estado){
	//aqui gestionaremos las salidas que no sean fugaces:
	switch (estado) {
		case REPOSO:
			//no hacemos nada:
			HAL_UART_Transmit(&huart2, (uint8_t*)"REPOSO...\n", strlen("REPOSO...\n"), HAL_MAX_DELAY);
			HAL_Delay(1000);
			break;
		case HX:
			//tenemos que enviar la información del HX:
			peso=hx711Weight(&hx, 10); //obtenemos el peso con 10 muestras;
			//sprintf(mensaje,"peso: %.1f g offset:%d bits pendiente: %.6f g/bits\n",peso,(int)hx.offset,hx.m);
			sprintf(mensaje,"H%d;%.1f;%d;%.6f\n",(int)((peso/hx.m)+hx.offset),peso,(int)hx.offset,hx.m);
			HAL_UART_Transmit(&huart2, (uint8_t*)mensaje, strlen(mensaje), HAL_MAX_DELAY);
			break;
		case OHM:
			//HAL_UART_Transmit(&huart2, (uint8_t*)"OHM...\n", strlen("OHM...\n"), HAL_MAX_DELAY);
			resis=Ohmnimetro(leerADC(&hadc1, HAL_MAX_DELAY), R);
			//sprintf(mensaje,"Valor de Resistencia:%.0f ohm\n",resis);
			sprintf(mensaje,"O%.0f\n",resis);
			HAL_UART_Transmit(&huart2, (uint8_t*)mensaje, strlen(mensaje), HAL_MAX_DELAY);
			HAL_Delay(1000);
			break;
		case INA:
			//leemos el adc y calculamos valor:
			//vina=salidaINA(leerADC(&hadc1, HAL_MAX_DELAY), vrefADC(&hadc1, ADC_CHANNEL_9, VREFINT_CAL, 1));
			vina=lecturaAMP(&myAMP, HAL_MAX_DELAY, 1024);
			//sprintf(mensaje,"Valor de INA:%.3f V\n",vina);
			//sprintf(mensaje,"I%.3f;%.3f;%.3f\n",vina,salidaINA(leerADC(&hadc1, HAL_MAX_DELAY), vrefADC(&hadc1, ADC_CHANNEL_9, VREFINT_CAL, 1)),myAMP.vref);
			//enviamos: I gramos; offset (voltios); factor de ganancia:
			sprintf(mensaje,"I%.3f;%.3f;%.2f\n",vina,myAMP.offset,myAMP.scale);
			HAL_UART_Transmit(&huart2, (uint8_t*)mensaje, strlen(mensaje), HAL_MAX_DELAY);
			HAL_Delay(1000);
			break;
		default:
			//no hacemos nada
			break;
	}
}
float Ohmnimetro(uint32_t adc_val, uint32_t Rserie){
	//función que calcula el valor de la resistencia
	//suponemos tensión Vdd cuyo valor en bits es 4095:
	float res;
	res=((4095.0f/(float)adc_val)-1)*Rserie;
	return res;
}

/*uint32_t leerADC(ADC_HandleTypeDef* hadc, uint32_t time){
	//función para leer por poll:
	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, time);
	return HAL_ADC_GetValue(hadc); //devolvemos el valor leeido
}*/
/*void CanalADC(ADC_HandleTypeDef* hadc, uint32_t CANAL){
	/*Con esta funcion se pretende cambiar el canal de un adc
	 * en un principio solo está pensado para adc con 1 solo canal
	 * */

	//estructura de inicio de canal:
	/*ADC_ChannelConfTypeDef sConfig = {0};

	sConfig.Channel = CANAL;
	sConfig.Rank = 1; //se supone que es solo de 1 canal luego siempre mismo rank
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES; //siempre mismos ciclos
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
	   Error_Handler();
	}
}*/
/*void SelecADC(ADC_HandleTypeDef* hadc, estados siguienteEstado){
	/*cuando se produzca un cambio de estado se usa esta función para seleccionar el canal del adc y hacer ajustes
	 * */
/*
	switch (siguienteEstado) {
		case HX:
			//seleccionamos el canal del HX si es que hay al final
			//ahora mismo canal 8:
			CanalADC(hadc, ADC_CHANNEL_8);
			HAL_Delay(1);
			break;
		case OHM:
			//seleccionamos el canal del ohmnimetro
			//ahora mismo canal 15:
			CanalADC(hadc, ADC_CHANNEL_15);
			HAL_Delay(1);
			break;
		case INA:
			//seleccionamos el canal del ina
			//ahora mismo canal 9:
			CanalADC(hadc, ADC_CHANNEL_9);
			HAL_Delay(1);
			break;
		default:
			break;
	}
}*/

/*float vrefADC(ADC_HandleTypeDef* hadc, uint32_t canal, uint16_t* pvref, uint8_t n){
	//parece ser que solo el ADC1 puede leer ese canal:

	float res=0;

	//cambiamos el canal al del vref:
	CanalADC(hadc, ADC_CHANNEL_VREFINT);
	//leemos n veces:
	for(uint8_t i=0; i<n; i++){
		res=res+(3.3f*(*pvref)/leerADC(hadc, HAL_MAX_DELAY));
	}
	res=res/n;

	//devolvemos el adc a su canal:
	CanalADC(hadc, canal);

	return res;

}*/

float salidaINA(uint32_t adc_val, float vref){
	//obtenemos el valor en V del ina:
	float res;

	res=vref*((2.0f*adc_val/4095)-1);

	return res;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim2);
  //inicializamos el hx711:
  hx711SetGain(&hx, 128);
  hx.gpioDT=DT_GPIO_Port;
  hx.pinDT=DT_Pin;
  hx.gpioSCK=SCK_GPIO_Port;
  hx.pinSCK=SCK_Pin;
  hx.offset=0;
  hx.m=1;
  //inicializamos el receptor de mandatos:
  msgInit(&consigna, '#', '@');

  //iniciamos la recepcion del uart en modo interrupcion:
  HAL_UART_Receive_IT(&huart2, (uint8_t*)&c, 1);

  //inicializamos el adc con su entrada:
  AMPInit(&myAMP, &hadc1, ADC_CHANNEL_9, 4095, VREFINT_CAL);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if(msgEndFlag(&consigna)==1){//si hay una consigna lista
		  msgRead(&consigna, mensaje); //sacamos del buffer el mensajem
		  nextEstado=siguienteEstado(mensaje, estado);
		  if(nextEstado!=estado){
			  //SelecADC(&hadc1, nextEstado);
			  estado=nextEstado;
			  HAL_UART_Transmit(&huart2, (uint8_t*)"...\n", strlen("...\n"), HAL_MAX_DELAY);
		  }
	  }

	  salidas(estado);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 83;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0xfffe;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : DT_Pin */
  GPIO_InitStruct.Pin = DT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SCK_Pin */
  GPIO_InitStruct.Pin = SCK_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SCK_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance==USART2){

		msgBuffer(&consigna, c);
		HAL_UART_Receive_IT(&huart2, (uint8_t*)&c, 1);
	}

}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
