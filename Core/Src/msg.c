/*
 * msg.c
 *
 *  Created on: May 5, 2021
 *      Author: halex
 */

#include "msg.h"

void msgInit(msg* m, char start, char end){
	/*
	 * en esta funcion se inicializa la estructura
	 * */
	m->n=0;			//ponemos a cero el numero de bits en el buffer
	m->start=start;
	m->end=end;
	m->endFlag=0;	//ponemos a cero la badera de mensaje listo
	m->state=0;		//nos situamos en el estado inicial (esperando char de inicio)

}

uint8_t msgEndFlag(msg* m){
	/*
	 * en esta funcion se comprueba si hay un mensaje listo para ser leido
	 * */
	return (m->endFlag); // 1 si está listo
}

void msgBuffer(msg* m, char c){
	/*
	 * En esta funcion se llena el buffer con los caracteres que llegan por el UART
	 * se empienza cuando llega char de start y se acaba cuando llega char de end
	 * */
	switch (m->state) {
		case 0: //esperando al char de inicio ('#' por ejemplo)
			if(c==m->start){
				m->n=0; //ponemos a 0 el contador
				m->endFlag=0; //ponemos a 0 la banderilla de fin de mensaje
				m->state=1; //cambiamos de estado
			}

			break;
		case 1:
			if(c==m->end){ //si nos ha llegado el ultimo caracter
				m->rxBuffer[m->n]='\0'; //agregamos el char null para convertirlo en un string
				(m->n)++; //incrementamos
				(m->n)=(m->n)%BUFFSIZE; //circular

				m->state=0; //volvemos al estado inicial
				m->endFlag=1; //banderilla de final de mensaje (igual lo cambio por mensaje listo)
				//igual meter n=0 pero lo vere luego
			}
			else if(c==m->start){ //si por algun casual error y vuelve a llegar char de inicio:
				m->n=0; //ponemos a 0 el contador
				m->endFlag=0; //ponemos a 0 la banderilla de fin de mensaje
			}
			else{ //si llega cualquier otro caracter que no sea el de final
				m->rxBuffer[m->n]=c; //metemos el caracter en el buffer
				(m->n)++; //incrementamos
				(m->n)=(m->n)%BUFFSIZE; //circular
			}
			break;
		default:
			break;
	}


}

void msgRead(msg* m, char* pntr){
	/*
	 * en esta funcion se lee el mensaje que hay almacenado, se baja la banderilla de mensaje listo
	 * */
	strcpy(pntr,m->rxBuffer);
	m->endFlag=0; //ya no está disponible el mensaje

}
